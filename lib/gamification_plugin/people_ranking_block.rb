class GamificationPlugin::PeopleRankingBlock < Block

  settings_items :limit, :type => :integer, :default => 5

  def self.description
    _('Gamification Ranking of Person By Context')
  end

  def help
    _('This block display te gamification rank of person by context.')
  end

  def self.pretty_name
    _('Gamification Ranking of Person By Profile')
  end

  def embedable?
    true
  end

  def cacheable?
    false
  end

  def content(args={})
    block = self
    proc do
      render :file => 'blocks/ranking', :locals => {:block => block}
    end
  end

end
