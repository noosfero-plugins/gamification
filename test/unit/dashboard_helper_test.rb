require_relative "../test_helper"

class DashboardHelperTest < ActionView::TestCase

  include GamificationPlugin::DashboardHelper

  def setup
    @environment = Environment.default
  end

  should 'return title for global badges' do
    owner = Environment.new
    assert_equal 'Badges', badges_title(owner)
  end

  should 'return title for organization badges' do
    owner = Organization.new(:name => 'organization')
    assert_equal 'Badges for organization', badges_title(owner)
  end

  should 'return badges grouped by owner' do
    environment = Environment.default
    expects(:environment).at_least_once.returns(environment)
    badge1 = GamificationPlugin::Badge.create!(:owner => fast_create(Organization))
    badge2 = GamificationPlugin::Badge.create!(:owner => environment)
    assert_equal [[badge2.owner, [badge2]], [badge1.owner, [badge1]]], grouped_badges
  end

  should 'return category of a score point' do
    categorization = GamificationPlugin::PointsCategorization.for_type(:vote_voter).first
    point_type = categorization.point_type
    score = Merit::Score.new
    score.category = categorization.id.to_s
    score.save!
    point = score.score_points.create!
    assert_equal "Voter", score_point_category(point)
  end

  should 'not include a profile with disabled gamification in ranking' do
    author = create_user('testuserauthor').person
    person1 = create_user('testuser1').person
    person2 = create_user('testuser2').person
    create_point_rule_definition('comment_author')
    article = create(TextArticle, :profile_id => author.id, :author_id => author.id)
    create(Comment, :source => article, :author_id => person1.id)
    3.times { create(Comment, :source => article, :author_id => person2.id) }
    person1.update_attribute(:gamification_plugin_disabled, true)
    assert_not_includes calc_ranking(person1, Time.zone.now.at_beginning_of_week), person1
  end

end
