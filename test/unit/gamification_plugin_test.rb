require_relative '../test_helper'

class GamificationPluginTest < ActiveSupport::TestCase

  def setup
    @plugin = GamificationPlugin.new
    @current_person = create_user('person').person
    @response = mock
  end

  attr_accessor :plugin, :current_person, :response

  should 'return user points and badges in user_data_extras' do
    assert_equal({:gamification_plugin => {:points => 0, :badges => [], :level => 0}}, instance_eval(&plugin.user_data_extras))
  end

  should 'not return gamification data in user_data_extras when disabled' do
    current_person.update_attribute(:gamification_plugin_disabled, true)
    assert_equal({}, instance_eval(&plugin.user_data_extras))
  end

  should 'render notifications for a profile' do
    response.expects(:status).returns(200)
    expects(:render).returns('render notifications')
    assert_equal 'render notifications', instance_eval(&plugin.body_ending)
  end

  should 'render nothing when there is no current person' do
    @current_person = nil
    assert_equal '', instance_eval(&plugin.body_ending)
  end

  should 'not render notifications when gamification is disabled for a profile' do
    current_person.update_attribute(:gamification_plugin_disabled, true)
    assert_equal '', instance_eval(&plugin.body_ending)
  end

end
