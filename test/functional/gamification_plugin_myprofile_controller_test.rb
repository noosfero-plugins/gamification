require_relative '../test_helper'

class GamificationPluginMyprofileControllerTest < ActionController::TestCase

  def setup
    @person = create_user('person').person
    @environment = Environment.default
    login_as(@person.identifier)
  end

  attr_accessor :person

  should 'disable gamification for profile' do
    person.update_attribute(:gamification_plugin_disabled, false)
    post :disable_gamification, profile: person.identifier
    assert person.reload.gamification_plugin_disabled
  end

  should 'enable gamification for profile' do
    person.update_attribute(:gamification_plugin_disabled, true)
    post :enable_gamification, profile: person.identifier
    assert !person.reload.gamification_plugin_disabled
  end

end
