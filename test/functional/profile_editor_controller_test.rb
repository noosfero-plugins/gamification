require_relative '../test_helper'

class ProfileEditorControllerTest < ActionController::TestCase

  def setup
    @person = create_user('person').person
    @environment = Environment.default
    @environment.enable_plugin(GamificationPlugin)
    login_as(@person.identifier)
  end

  attr_accessor :person

  should 'display button to enable gamification when it is disabled' do
    person.update_attribute(:gamification_plugin_disabled, true)
    get :edit, profile: person.identifier
    assert_select '#gamification_enable'
  end

  should 'display button to disable gamification when it is enabled' do
    person.update_attribute(:gamification_plugin_disabled, false)
    get :edit, profile: person.identifier
    assert_select '#gamification_disable'
  end

end
