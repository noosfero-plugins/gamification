class AddGamificationDisabledFieldToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :gamification_plugin_disabled, :boolean, :default => false
  end
end
