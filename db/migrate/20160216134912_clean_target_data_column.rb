class CleanTargetDataColumn < ActiveRecord::Migration
  
  def up
    models = {'article' => 'Article', 'comment' => 'Comment', 'vote' => 'Vote',
      'friendship' => 'Friendship', 'profile' => 'Profile',
      'articlefollower' => 'ArticleFollower'}

    Merit::Action.where("target_model NOT IN (?)", models.keys).find_each do |action|
      next if action.target_data.blank?
      obj = YAML.load(action.target_data) rescue nil
      unless obj.nil?
        action.update_attribute(:target_model, obj.class.respond_to?(:base_class) ? obj.class.base_class.name : obj.class.name)
      end
    end

    models.each do |old_name, new_name|
      Merit::Action.where(target_model: old_name).update_all(target_model: new_name)
    end

    Merit::Action.update_all(target_data: nil)
  end
  
  def down
    puts "Warning: cannot restore target_data"
  end
  
end
