class GamificationPluginProfileController < ProfileController

  before_filter :verify_gamification_enabled

  def dashboard
    @target = profile
    render 'gamification/dashboard'
  end

  protected

  def verify_gamification_enabled
    render_access_denied if profile.gamification_plugin_disabled
  end

end
