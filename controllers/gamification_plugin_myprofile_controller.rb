class GamificationPluginMyprofileController < MyProfileController

  def enable_gamification
    toggle_gamification(false)
    session[:notice] = _('Gamification enabled')
  end

  def disable_gamification
    toggle_gamification(true)
    session[:notice] = _('Gamification disabled')
  end

  protected

  def toggle_gamification(disabled = false)
    current_person.update_attribute(:gamification_plugin_disabled, disabled)
    redirect_to :controller => 'profile_editor', :action => 'edit'
  end

end
